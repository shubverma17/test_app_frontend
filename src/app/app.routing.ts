import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoChartsComponent } from './article/demoCharts.component';


const appRoutes: Routes = [
    { path: 'articles/:id', component: DemoChartsComponent }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
