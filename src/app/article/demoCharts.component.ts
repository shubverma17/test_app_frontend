import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DemoChartServices } from './demoCharts.service';
import { Response } from '@angular/http';

/**
 * A simple component, which fetches charts from HTTP API and displays it.
 */
@Component({
    selector: 'demo-chart',
    templateUrl: './democharts.html',
    styleUrls: ['./democharts.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [DemoChartServices]
})
export class DemoChartsComponent implements OnInit, OnDestroy {
    private article: DemoChartServices;
    private error: Response;
    private isLoading: boolean = true;

    options;
    data;

    constructor(
        private route: ActivatedRoute,
        private demochartservice: DemoChartServices
    ) {

    }

    /**
     * TODO: Note about non-observable param
     */
    ngOnInit(): void {

        this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration: 500,
        xAxis: {
          axisLabel: 'X Axis'
        },
        yAxis: {
          axisLabel: 'Y Axis',
          axisLabelDistance: -10
        }
      }
    }
    this.demochartservice.getAll().subscribe(
        data  => {console.log(data);
            this.data = [
            {
                key:"Cumulative Return",
                values: data.chartvalues
            }
            ]},
        error => this.error = error,
        ()      => this.isLoading = false

    );
    
    }

    ngOnDestroy(): void {}
}
